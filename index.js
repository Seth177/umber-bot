let umber = require('./umber.js');
const WebS = require("ws");
const chalk = require('chalk');
const config = require('../config');

umber()
    .then(u => u.login()).catch(() => null);

const wss = new WebS.Server({
    host: "0.0.0.0",
    port: 8080
});

wss.on('connection', function connection(ws) {
    console.log(chalk.red("DevTest Connected"));
    ws.on('message', function incoming(message) {
        let str = JSON.parse(message);
        if (str.devTestPassword == config.devTestPassword) {
            switch (str.command) {
                case 'login':
                    umber = require('./umber.js');
                    umber().then(u => u.login().catch(() => null));
                    break;
                case 'logout':
                    umber()
                        .then(u => {
                            u.logout();
                            setTimeout(() => {
                                umber = null;
                            }, 2000);
                        }).catch(() => null);
                    break;

            }
        } else return;
    });
});