const _command = {};
const Datastore = require('nedb');
const chalk = require('chalk');

let locationDB = new Datastore({
	filename: './Databases/raidLOC.db',
	autoload: true
});

let raids = new Datastore();

const askQuestion = (msg, filter) => {
	return new Promise((fulfill, reject) => {
		msg.channel.awaitMessages(filter, {
			max: 1,
			time: 10000,
			errors: ['time']
		}).then(collected => {

			fulfill(collected.first());
		}).catch(() => msg.channel.send({
				embed: {
					color: colors[Math.floor(Math.random() * colors.length)],
					title: `Umber tried to do something!`,
					description: `but nothing happaned!`
				}
			})
			.then(c => reject(c)));
	});
};

const colors = [0x020303, 0x4A575D, 0x566574, 0xF4D995, 0xE07451, 0x2e9afe];

module.exports = (client) => {
	_command.name = `raid`;
	_command.shortName = 'r';
	_command.description = `This is for raiding and such`;
	_command.usage = `raid`;
	_command.do = (msg) => {
		return new Promise((fulfill) => {
			let perms = msg.channel.permissionsFor(msg.member);
			let args = msg.content.split(' ');
			console.log(chalk.yellow(args));
			if (args[1] == 'mloc' || args[1] == 'managelocations') {
				const filter = m => m.member == msg.member;
				if (msg.member.roles.find(val => val.name === "Administrator") || msg.member.user.id == '70921043782402048') {
					msg.channel.send({
						embed: {
							color: colors[Math.floor(Math.random() * colors.length)],
							title: `What would you like to do?`,
							fields: [{
								name: 'add',
								value: `Add a location`,
								inline: true
							}, {
								name: 'delete',
								value: `Deletes a location`,
								inline: true
							}, {
								name: 'update',
								value: `Edit a location`,
								inline: true
							}, {
								name: 'reset',
								value: `Resets a locations statistics`,
								inline: true
							}]
						}
					});
					askQuestion(msg, filter)
						.then(msg => {
							msg.delete();
							switch (msg.content) {
								case 'add':
									let gym = {
										raidCount: 0,
										raidParties: 0
									};
									msg.channel.send(`What is the name of the GYM?`).then(m => m.delete(10000));
									askQuestion(msg, filter).then((msg) => {
										msg.delete(1000);
										gym.name = msg.content;
										msg.channel.send(`Is this gym a  EX gym (yes, no)`).then(m => m.delete(10000)).catch(() => null);
										askQuestion(msg, filter).then((msg) => {
											msg.delete(1000);
											if (msg == 'yes') gym.ex = true;
											else gym.ex = false;
											msg.channel.send(`What is the address of this gym?`).then(m => m.delete(10000)).catch(() => null);
											askQuestion(msg, filter).then((msg) => {
												msg.delete(1000);
												gym.address = msg.content;
												msg.channel.send(`What is a google maps link to this location?`).then(m => m.delete(10000)).catch(() => null);
												askQuestion(msg, filter).then((msg) => {
													msg.delete(1000);
													gym.link = msg.content;
													locationDB.insert(gym);
													msg.channel.send({
															embed: {
																color: colors[Math.floor(Math.random() * colors.length)],
																title: `Umber used Manage Locations, and added a gym!`,
																fields: [{
																	name: 'Gym Name:',
																	value: gym.name
																}, {
																	name: 'EX:',
																	value: gym.ex
																}, {
																	name: 'Address:',
																	value: gym.address
																}, {
																	name: 'Gym Link:',
																	value: gym.link
																}]
															}
														})
														.then(c => {
															fulfill(c);
														});
												}).catch(() => null);
											}).catch(() => null);
										}).catch(() => null);
									}).catch(() => null);
									break;
								case 'delete':
									msg.channel.send({
											embed: {
												color: colors[Math.floor(Math.random() * colors.length)],
												title: `Umber used Manage Locations!`,
												fields: [{
													name: 'Recieved:',
													value: msg.content
												}]
											}
										})
										.then(c => {
											c.delete(10000);
											fulfill(c);
										});
									break;
								case 'update':
									msg.channel.send({
											embed: {
												color: colors[Math.floor(Math.random() * colors.length)],
												title: `Umber used Manage Locations!`,
												fields: [{
													name: 'Recieved:',
													value: msg.content
												}]
											}
										})
										.then(c => {
											c.delete(10000);
											fulfill(c);
										});
									break;
								case 'reset':
									msg.channel.send({
											embed: {
												color: colors[Math.floor(Math.random() * colors.length)],
												title: `Umber used Manage Locations!`,
												fields: [{
													name: 'Recieved:',
													value: msg.content
												}]
											}
										})
										.then(c => {
											c.delete(10000);
											fulfill(c);
										});
									break;
								default:
									msg.channel.send({
											embed: {
												color: colors[Math.floor(Math.random() * colors.length)],
												title: `Umber tried to do something!`,
												description: `but nothing happaned!`
											}
										})
										.then(c => {
											c.delete(10000);
											fulfill(c);
										});
									break;
							}
						});
				} else msg.channel.send({
						embed: {
							color: colors[Math.floor(Math.random() * colors.length)],
							title: `Umber tried to Manage Locations!`,
							description: `but its not effective!`
						}
					})
					.then(c => {
						c.delete(10000);
						fulfill(c);
					});
			} else if (args[1] == 'g' || args[1] == 'gyms') {
				locationDB.find({}, (e, r) => {
					let names = [];
					r.forEach((i) => {
						names.push(i.name);
					});

					msg.channel.send({
						embed: {
							color: colors[Math.floor(Math.random() * colors.length)],
							title: `Umber points out gym locations!`,
							description: `${names.join(' | ')}`
						}
					});
				});
			} else {
				const filter = m => m.member == msg.member;
				msg.channel.send({
					embed: {
						color: colors[Math.floor(Math.random() * colors.length)],
						title: `What would you like to do?`,
						fields: [{
							name: 'add',
							value: `Report a new raid`,
							inline: true
						}, {
							name: 'remove',
							value: `Removes a raid listing, shouldent be needed as normally raids will expire in like 2 hours \n(original poster or admin only)`,
							inline: true
						}, {
							name: 'update',
							value: `Updates a raids information \n(original poster or trusted member only)`,
							inline: true
						}, {
							name: 'reset',
							value: `Resets a raid party and removes all members from it \n(original poster, or admin only)`,
							inline: true
						}, {
							name: 'noshow',
							value: `Reports a party member as a no show \n(original poster, or trusted member only)`,
							inline: true
						}]
					}
				}).then(c => {
					c.delete(10000);
					fulfill(c);
				}).catch(() => null);
				askQuestion(msg, filter)
					.then(msg => {
						msg.delete();
						switch (msg.content) {
							case 'add':
								let raid = {
									raidParty: [],
									raidStartTime: 0,
									raidEndTime: 0
								};
								msg.channel.send({
										embed: {
											color: colors[Math.floor(Math.random() * colors.length)],
											title: `Umber used Raid!`,
											description: `But it isn\'t ready yet!`
										}
									})
									.then(c => {
										c.delete(10000);
										fulfill(c);
									}).catch(() => null);
								break;
							case 'remove':
								msg.channel.send({
										embed: {
											color: colors[Math.floor(Math.random() * colors.length)],
											title: `Umber used raid!`,
											fields: [{
												name: 'Recieved:',
												value: msg.content
											}]
										}
									})
									.then(c => {
										c.delete(10000);
										fulfill(c);
									}).catch(() => null);
								break;
							case 'update':
								msg.channel.send({
										embed: {
											color: colors[Math.floor(Math.random() * colors.length)],
											title: `Umber used raid!`,
											fields: [{
												name: 'Recieved:',
												value: msg.content
											}]
										}
									})
									.then(c => {
										c.delete(10000);
										fulfill(c);
									}).catch(() => null);
								break;
							case 'reset':
								msg.channel.send({
										embed: {
											color: colors[Math.floor(Math.random() * colors.length)],
											title: `Umber used raid!`,
											fields: [{
												name: 'Recieved:',
												value: msg.content
											}]
										}
									})
									.then(c => {
										c.delete(10000);
										fulfill(c);
									}).catch(() => null);
								break;
							case 'noshow':
								msg.channel.send({
										embed: {
											color: colors[Math.floor(Math.random() * colors.length)],
											title: `Umber used raid!`,
											fields: [{
												name: 'Recieved:',
												value: msg.content
											}]
										}
									})
									.then(c => {
										c.delete(10000);
										fulfill(c);
									}).catch(() => null);
								break;
							default:
								msg.channel.send({
										embed: {
											color: colors[Math.floor(Math.random() * colors.length)],
											title: `Umber tried to do something!`,
											description: `but nothing happaned!`
										}
									})
									.then(c => {
										c.delete(10000);
										fulfill(c);
									}).catch(() => null);
								break;
						}
					}).catch(() => null);
			}
		});
	};
	return _command;
};