const _command = {};

module.exports = (client) => {
	_command.name = `ping`;
	_command.shortName = null;
	_command.description = `This is a dummy command to act as a example/placeholder, it is simply for testing`;
	_command.usage = `ping`;
	_command.do = (message) => {
		return new Promise((fulfill, reject) => {
			message.channel.send('pong')
				.then(c => fulfill(c))
				.catch(e => reject(e));
		});
	};
	return _command;
};