const config = require('../config');
var stdin = process.openStdin();
const umber = require('./umber.js');
const WebS = require('ws');

const ws = new WebS('ws://ratchtnet.com:8080');

ws.on('open', function open() {
    console.log("Connected");
});

ws.on('message', function incoming(data) {
    console.log(JSON.parse(data));
});

stdin.addListener("data", function (d) {
    switch (d.toString().trim()) {
        case 'login':
            ws.send(`{"devTestPassword": "${config.password}", "command": "login"}`);
            break;
        case 'logout':
            ws.send(`{"devTestPassword": "${config.password}", "command": "logout"}`);
            break;
        case 'test':
            umber()
                .then(u => u.login());
            break;
        case 'endtest':
            umber()
                .then(u => u.logout());
            break;
        default:
            eval(d);
            break;

    }
});