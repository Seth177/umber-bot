const Discord = require('discord.js');
const client = new Discord.Client();
const chalk = require('chalk');
const fs = require('fs');
const config = require('../config.json');

const commands = {};

const help = msg => {
    return msg;
};

commands.run = (command, msg, args) => {
    return new Promise((fulfill, reject) => {
        commands[command].do(msg, args)
            .then(result => fulfill(result))
            .catch(error => reject(error));
    });
};
const commandLoad = () => {
    let commandFiles = fs.readdirSync(__dirname + '/commands');

    commandFiles.forEach((file) => {
        if (file.startsWith('index')) return;
        let fileName = file.replace(/\.js/, '');
        commands[fileName] = require(`${__dirname}/commands/${file}`)(client);
        if (commands[fileName].shortName != null) commands[commands[fileName].shortName] = commands[fileName];
    });
};

client.on('ready', () => {
    commandLoad();
    console.log(chalk.magenta(`Logged in as ${client.user.tag}!`));
});

client.on('message', msg => {
    if (msg.content.startsWith('u!')) {
        msg.content = msg.content.replace('u!', '');
        let command = msg.content.split(' ');
        if (command[0] != 'help')
            commands.run(command[0], msg, command)
            .then(result => {
                return result;
            })
            .catch(error => console.log(error));
        else help(msg);
    } else return;
});

module.exports = () => {
    return new Promise((resolve) => {
        resolve({
            logout: () => client.destroy(),
            login: () => client.login(config.umber)
        });

    });
};